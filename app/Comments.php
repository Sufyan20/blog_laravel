<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable =[
        'comment','user_id','post_id',
    ];

    protected $hidden = [
        'remember_token',
    ];

    public function User(){
      return  $this->belongsTo(User::class);
    }
    public function Post(){
       return $this->belongsTo(Post::class);
    }
}
