<?php

namespace App\Http\Controllers;
use App\Comments;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Post;
use Illuminate\Pagination\LengthAwarePaginator;
class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

//        $posts = Post::all();
//        $post = Post::find(2);
//        $p = $post->user()->first()->fname;
//        dd($p);

        $posts = Post::Orderby('id','desc')->Paginate(5);

        return view('home')->with('posts',$posts);
    }

    public function search(Request $request){
     $search = $request->get('search');
     $posts =  Post::where('post_title','like','%'.$search.'%')->Paginate(5);

     return view('home')->with('posts', $posts);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Bloger.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'post_title' => 'required|string|max:255',
            'post_details' => 'required|string|max:600',
        ]);
        if($request->hasFile('post_img')){


            $imageName = $request->post_img->getClientOriginalName();
            $request->post_img->move(public_path('images'),$imageName);
        }
        else{
            die('Img not found');
        }
        $post = new Post();
        $post->post_title = $request->post_title;
        $post->post_details = $request->post_details;
        $post->post_img = $request->post_img->getClientOriginalName();
        $post->user_id = $request->user_id;
        $post->save();

          return redirect('home');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $post = Post::find($id);

        $uid =$post->user_id;
        $p_user = User::find($uid);

//        $c = Comments::find(1);
//        $cd = $c->user()->first()->fname;
//        dd($cd);
        $comment = new Comments();

        $comments = $comment->where('post_id','=',$id)->get();


        return view('Bloger.show' ,['post' => $post, 'comments' => $comments, 'p_user' => $p_user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('edit')->with('post',$post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('post_img')){
            $imageName = $request->post_img->getClientOriginalName();
            $request->post_img->move(public_path('images'),$imageName);
        }
        else{
            die('Img not found');
        }
        $post =  Post::find($id);
        $post->post_title = $request->post_title;
        $post->post_details = $request->post_details;
        $post->post_img = $request->post_img->getClientOriginalName();
        $post->user_id = $request->user_id;
        $post->created_at = now();

        $post->save();

        return redirect('home');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $post = Post::find($id);
        $post->delete();

          return redirect('home');


    }
}
