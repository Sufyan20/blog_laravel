<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){
        return view('Admin.index');
    }
    Public function usersList(){
        $users = User::all();
        return view('Admin.userslist')->with('users', $users);
    }
    Public function postsList(){
        $posts = Post::all();
        return view('Admin.postslist')->with('posts', $posts);
    }
}
