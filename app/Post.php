<?php

namespace App;
use Illuminate\Foundation\Auth;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

public function Comments(){
   return $this->hasMany(Comments::class);
}
    public function User(){
       return $this->belongsTo(User::class);
    }
    protected $fillable = [
        'post_title','post_details','post_img','user_id',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    public $timestamps = TRUE;
}
