<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('home','HomeController');
Route::resource('Comment','CommentController');
Route::get('editUser/{id}', 'UserController@edit')->name('edituser');
Route::resource('user','UserController');
Route::get('/search' ,'HomeController@search')->name('search');
Route::get('/profile' , 'UserController@profile')->name('profile');
Route::delete('/destroyUser', 'UserController@destroy')->name('DeleteUser');

Route::get('/admin' ,'AdminController@index')->name('Admin');
Route::get('/admin/users', 'AdminController@usersList')->name('userslist');
Route::get('/admin/posts', 'AdminController@postsList')->name('postslist');
