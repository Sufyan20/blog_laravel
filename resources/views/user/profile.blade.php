@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row pt-5 justify-content-center">
            <div class="col-md-10">
               <div class="row">
                   <div class="col-md-5">
                       <img src="{{asset('images')}}/{{Auth::User()->userimg}}" class="img-responsive img-circle" style="height: 300px; width: 300px;" alt="#">

                   </div>


                   <div class="col-md-2 pt-4">
                       <h4 class="pt-3">First Name :</h4>
                       <h4 class="pt-3">Last Name :</h4>
                       <h4 class="pt-3">Email  :</h4>
                       <h4 class="pt-3">Gender  :</h4>
                       <h4 class="pt-3">Birthday  :</h4>
                       <button class="btn btn-info mt-5 p-3">
                           <a href="{{url('editUser/'.Auth::user()->id)}}"><strong class="text-white">Edit Profile</strong>  </a>
                       </button>
                   </div>
                   <div class="col-md-3 pt-4">
                       <h4 class="pt-3">{{Auth::user()->fname}}</h4>
                       <h4 class="pt-3">{{Auth::user()->lname}}</h4>
                       <h4 class="pt-3">{{Auth::user()->email}}</h4>
                       <h4 class="pt-3">{{Auth::user()->gender}}</h4>
                       <h4 class="pt-3">{{Auth::user()->dob}}</h4>
                       <form method="post" action="{{route('DeleteUser')}}">
                           {{method_field('DELETE')}}
                           {{csrf_field()}}
                           <button type="submit" class="btn btn-danger mt-5 p-3">
                               Delete Account
                           </button>
                       </form>
                   </div>
                   <div class="col-md-10 pt-5">
                       <h2> {{Auth::user()->fname}} Posts  </h2>
                   </div>
                   <div class="col-md-10 pt-5">
                       @if (count($posts) > 0)
                           @foreach($posts as $post)
                               <small>Posted by : {{$post->user()->first()->fname}}</small>
                               <div class="single-list flex-row d-flex">
                                   <div class="thumb">
                                       <img src="{{asset('images')}}\{{$post->post_img}}" alt="" class="img-responsive" style="width: 250px; height: 250px">
                                   </div>
                                   <div class="detail">

                                       <a href="home/{{$post->id}}"><h4 class="pb-20">{{$post->post_title}}<br>
                                           </h4></a>
                                       <p>
                                           {!! $post->post_details !!}
                                       </p>
                                       <small> {{$post->updated_at}}</small>
                                       <p class="footer pt-20">

                                           <i class="ml-20 fa fa-comment-o" aria-hidden="true"></i> <a href="{{url('home/'.$post->id)}}">
                                               Comments :   {{count($post->Comments()->get())}}

                                           </a>
                                       </p>
                                   </div>
                               </div>


                           @endforeach
                           @endif

                   </div>

               </div>

            </div>
        </div>
    </div>



    @endsection
