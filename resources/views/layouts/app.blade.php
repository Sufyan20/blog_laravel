<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('img/fav.png')}}">
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <!-- Site Title -->
    <title>Blogger</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="{{asset('css/css/linearicons.css')}}">
    <link rel="stylesheet" href="{{asset('css/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset("css/css/font-awesome.min.css")}}">
    <link rel="stylesheet" href="{{asset('css/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/css/main.css')}}">
    <style>

            .navbar-nav>li>a {

                padding-top: 0px;
                padding-bottom: 0px;
            }
    </style>
</head>
<body>
<!-- Start Header Area -->
<header class="default-header" style="position: relative;">
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a class="navbar-brand" href="{{route('home.index')}}">
                <img src="{{asset('img/logo.png')}}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupportedContent">
                <ul class="navbar-nav scrollable-menu">
                    <li><a href="{{route('home.index')}}"> Home</a></li>
                    <li><a href="{{route('home.create')}}">Add Post</a></li>
                    <li><a href="about">About Us</a></li>
                    <li><a href="#team">team</a></li>
                    <!-- Dropdown -->
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle icon glyphicon-user" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->fname }}
                                </a>

                                <ul class="dropdown-menu">
                                    <li class="list-group-item">
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="{{url('profile')}}"> Profile </a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="{{url('editUser/'.Auth::user()->id)}}"> Edit Profile </a>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- End Header Area -->




        @yield('content')






<script src="{{asset('js/js/vendor/jquery-2.2.4.min.js')}}"></script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'article-ckeditor' );
</script>
<script src="{{asset('js/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('js/js/parallax.min.js')}}"></script>
<script src="{{asset('js/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('js/js/jquery.sticky.js')}}"></script>
<script src="{{asset('js/js/main.js')}}"></script>
<script src="{{asset('js/js/vendor/bootstrap.min.js')}}"></script>
</body>
</html>
