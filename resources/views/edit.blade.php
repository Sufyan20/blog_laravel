@extends('layouts.app')


@section('content')


    <div class="container">
        @if($post)
        <form enctype="multipart/form-data" method="post" action="{{action('HomeController@update',  ['id' => $post->id])}}">
            {{ csrf_field() }}
            <div class="card">
                <div class="card-header"> <h3>Add Your Post </h3></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="row">

                                <div class="col-md-3 pt-4">
                                    <label for="title">
                                        Post Title :
                                    </label>
                                </div>
                                <div class="col-md-9 pt-4">
                                    <input type="text" class="form-control" name="post_title" value="{{$post->post_title}}" placeholder="Post Title" required>
                                </div>

                                <div class="col-md-3 pt-4">
                                    <label for="details">
                                        Post Details :
                                    </label>
                                </div>
                                <div class="col-md-9 pt-4">
                                    <textarea class="form-control" name="post_details"  rows="8" id="details" placeholder="Add Post Details" required>{{$post->post_details}}</textarea>

                                </div>
                                <div class="col-md-3 pt-4">
                                    <div class="col-md-3">
                                        <label for="img">
                                            <strong>Select image :</strong>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-9 pt-4">

                                    <input type="file" name="post_img" value="{{$post->post_img}}" id="img" required>


                                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                </div>


                                <div class="col-lg-3"> </div>
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-info">Update Post</button>
                                </div>
                                {{method_field('PATCH')}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
    @else <h2>post id not found</h2>
    @endif




@endsection
