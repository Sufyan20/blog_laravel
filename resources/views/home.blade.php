@extends('layouts.app')

@section('content')

<style>
    /*.pagination li {*/
    /*    margin: 10px;*/
    /*    font-size: 20px;*/
    /*    font-family: serif;*/
    /*    font-weight: bold;*/
    /*    color: #5f4a4a;*/
    /*    background-color: #e1d5e4;*/
    /*    padding: 10px;*/
    /*    border-radius: 9px;*/
    /*}*/
    /*.pagination a {*/
    /*    border: none;*/
    /*    color: #100f0f;*/
    /*}*/
</style>
    <!-- Start top-section Area -->
    <section>
        <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/8/8d/Yarra_Night_Panorama%2C_Melbourne_-_Feb_2005.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/8/8d/Yarra_Night_Panorama%2C_Melbourne_-_Feb_2005.jpg" class="d-block w-100" alt="...">
                </div>

                <div class="carousel-item">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/8/8d/Yarra_Night_Panorama%2C_Melbourne_-_Feb_2005.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/8/8d/Yarra_Night_Panorama%2C_Melbourne_-_Feb_2005.jpg" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
    <section class="search-sec">



        <div class="container">
            <form action="{{route('search')}}" method="get" novalidate="novalidate">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row justify-content-center">
                            <div class="col-lg-5 col-md-5 col-sm-12 p-0">
                                <input type="text" class="form-control search-slt" name="search" placeholder="Search Here (by title)">
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                                <button type="submit" class="btn btn-danger wrn-btn">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <!-- End top-section Area -->

    <!-- Start post Area -->

        <!-- Start post Area -->
        <section class="post-area" >
            <div class="container">
                <div class="row justify-content-center d-flex bg-light p-0">
                    <div class="col-lg-8 p-0">
                        <div class="post-lists search-list">


                            @if (count($posts) > 0)
                                @foreach($posts as $post)
                                       <small>
                                           <img src="{{asset('images')}}/{{$post->user()->first()->userimg}}" alt="#" style="height: 60px; width: 60px; border-radius: 30px;">
                                          <strong>{{$post->user()->first()->fname}}  {{$post->user()->first()->lname}}</strong>

                                    </small>

                                    <div class="single-list flex-row d-flex">
                                        <div class="thumb">
                                            <img src="{{asset('images')}}\{{$post->post_img}}" alt="" class="img-responsive" style="width: 250px; height: 250px">
                                        </div>
                                        <div class="detail">

                                            <a href="home/{{$post->id}}"><h4 class="pb-20">{{$post->post_title}}<br>
                                                    </h4></a>
                                            <p>
                                                {!! $post->post_details !!}
                                            </p>
                                            <small> {{$post->updated_at}}</small>
                                            <p class="footer pt-20">

                                                   <i class="ml-20 fa fa-comment-o" aria-hidden="true"></i> <a href="{{url('home/'.$post->id)}}">
                                                  Comments :   {{count($post->Comments()->get())}}

                                                </a>
                                            </p>
                                        </div>
                                    </div>


                                @endforeach

                                    {{$posts->links()}}


                        </div>

                    </div>

                    <div class="col-lg-4 sidebar-area">

                        <div class="single_widget about_widget">
                            <img src="{{asset('images')}}/{{Auth::user()->userimg}}" style="height: 90px;width: 90px;" alt="">
                            <h2 class="text-uppercase">{{Auth::user()->fname}}  {{Auth::user()->lname}} </h2>
                            <p>
                                MCSE boot camps have its supporters and
                                its detractors. Some people do not understand why you should have to spend money
                            </p>
                            <div class="social-link">
                                <a href="#"><button class="btn"><i class="fa fa-facebook" aria-hidden="true"></i> Like</button></a>
                                <a href="#"><button class="btn"><i class="fa fa-twitter" aria-hidden="true"></i> follow</button></a>
                            </div>
                        </div>



                        <div class="single_widget recent_widget">

                            <h4 class="text-uppercase pb-20">Recent Posts</h4>
                            <div class="active-recent-carusel">

                                @foreach($posts as $post)
                                <div class="item">
                                    <img src="{{asset('images/')}}/{{$post->post_img}}" alt="">
                                    <p class="mt-20 title text-uppercase"><a href="home/{{$post->id}}">{{$post->post_title}} </a></p>
                                    <p>{{$post->created_at}} <span> <i class="fa fa-heart-o" aria-hidden="true"></i>
                                    06 <i class="fa fa-comment-o" aria-hidden="true"></i>02</span></p>
                                </div>
                                @endforeach

                            </div>
                        </div>
                        <div class="single_widget cat_widget">
                            <h4 class="text-uppercase pb-20">post categories</h4>
                            <ul>
                                <li>
                                    <a href="#">Technology <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Lifestyle <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Fashion <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Art <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Food <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Architecture <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Adventure <span>37</span></a>
                                </li>
                            </ul>
                        </div>

                        <div class="single_widget tag_widget">
                            <h4 class="text-uppercase pb-20">Tag Clouds</h4>
                            <ul>
                                <li><a href="#">Lifestyle</a></li>
                                <li><a href="#">Art</a></li>
                                <li><a href="#">Adventure</a></li>
                                <li><a href="#">Food</a></li>
                                <li><a href="#">Technology</a></li>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Adventure</a></li>
                                <li><a href="#">Food</a></li>
                                <li><a href="#">Technology</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    @else <h3>No record</h3>
    @endif
        <!-- End post Area -->
    <!-- End post Area -->


@endsection
