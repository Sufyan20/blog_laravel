@extends('layouts.layout')

@section('content')
    <div class="clearfix"></div>
<div class="container">
    <div class="row justify-content-between">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading text-center "><h2>Register</h2></div>

                <div class="panel-body">
                    <form class="form-horizontal" enctype="multipart/form-data"  method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('userimg') ? ' has-error' : '' }}">
                            <div class="col-md-7">
                                <input type="file" name="userimg" class="btn btn-default pull-right pt-5" required>
                            </div>
                            <div class="col-lg-4 col-md-4">
                      <img src="{{asset('images')}}/avatar7.png" alt="s" class="img-responsive pull-right" style="height: 150px; width: 150px;">
                            </div>

                        </div>


                        <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">

                            <label for="fname" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input id="fname" type="text" class="form-control" name="fname" value="{{ old('name') }}" placeholder="First Name" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong> {{ $errors->first('fname') }} </strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('lname') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input id="lname" type="text" class="form-control" name="lname" value="{{ old('lname') }}" placeholder="Last Name" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="dob" class="col-md-4 control-label">Date Of Birth</label>

                            <div class="col-md-6">
                                <input id="dob" type="date" class="form-control" name="dob" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                               <input type="hidden" name="type" value="user">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gender" class="col-md-4 control-label">Select Gender</label>

                            <div class="col-md-6">
                                <input type="radio" style="margin: 10px;" class="radio-inline" name="gender" value="Male" required> <strong>Male</strong>

                                <input type="radio" style="margin: 10px;" class="radio-inline" name="gender" value="Female" required>

                                <strong>Female</strong>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
