@extends('layouts.app');
@section('content')
<script>
    $('.editPost').on('click', function () {
        // $('#posttitle').val($(this).siblings('.editcomntcontent').val());
        // $('#CommentId').val($(this).siblings('.comntid').val());
        // $('#Usid').val($('.usrid').val());
        // $('#Poid').val($('.pstid').val());


        $('#editPostModal').modal('show');
    });
</script>
    <div class="modal fade" id="editPostModal" tabindex="-1" role="dialog" aria-labelledby="editPostModalTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="editPostModalTitle">Update Comment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">

                    <input type="hidden" name="editCommentId" id="CommentId">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="posttitle">Add Comment.</label>
                            <input type="text" id="posttitle" name="commentContnt" class="form-control" value=""
                                   placeholder="Enter Post Title">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" id="updatePost" name="updateComment" class="btn btn-primary">Save changes
                        </button>
                        <!-- To get the User and Post Id-->
                        <input id="Usid" type="hidden" name="uid" class="uid">
                        <input id="Poid" type="hidden" name="pid" class="pid">
                    </div>
                </form>
            </div>
        </div>
    </div>


    <button id="editbtn" type="button" name="editBtn" data-toggle="modal" class="editPost btn">
      dddd
    </button>





    <!-- Start post Area -->
    <div class="post-wrapper pt-80">
        <!-- Start post Area -->
        <section class="post-area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        @if($post)
                            <div class="single-page-post">
                                <img class="img-fluid img-responsive" src="{{asset('images')}}\{{$post->post_img}}" style="width: 100%;"  alt="">
                                <div class="top-wrapper ">

                                    <div class="row d-flex justify-content-between">
                                        <h2 class="col-lg-8 col-md-12 text-uppercase">
                                            {{$post->post_title}}
                                            <br>

                                        </h2>

                                        <div class="col-lg-4 col-md-12 right-side d-flex justify-content-end">
                                            <div class="desc">
                                               Posted by : <h2>{{$p_user->fname}} {{$p_user->lname}}</h2>
                                              On :   <h3>{{$post->created_at}}</h3>
                                            </div>
                                            <div class="user-img">
                                                <img src="{{asset('images')}}\{{$p_user->userimg}}" alt="#"
                                                     style="width: 80px; height: 70px; border-radius: 5px;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tags">
                                    <ul>
                                        @if($post->user_id === Auth::user()->id)
                                            <li class="bg-info" style="padding: 10px;border-radius: 13px; margin: 5px;">
                                                <a href="{{url('home/'.$post->id.'/edit')}}" style="font-size: 15px; color:white;">Edit Post</a></li>
                                            <li style="margin: 5px;" class="ml-10">
                                                <form method="post" action="{{route('home.destroy' ,$post->id)}}">
                                                    {{method_field('DELETE')}}
                                                    {{csrf_field()}}
                                                    <button type="submit" class="btn btn-danger ">
                                                        Delete post
                                                    </button>
                                                </form>
                                            </li>
                                                  @endif
                                                <li> </li>
                                    </ul>
                                </div>
                                <div class="single-post-content">
                                    <p>
                                        MCSE boot camps have its supporters and its detractors. Some people do not
                                        understand why you should have to spend money on boot camp when you can get the
                                        MCSE study materials yourself at a fraction of the camp price.
                                    </p>

                                </div>
                                <div class="bottom-wrapper">
                                    <div class="row">
                                        <div class="col-lg-4 single-b-wrap col-md-12">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            lily and 4 people like this
                                        </div>
                                        <div class="col-lg-4 single-b-wrap col-md-12">
                                            <i class="fa fa-comment-o" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>

                                <form action="{{route('Comment.store')}}" method="post">
                                {{ csrf_field() }}
                                <!-- Commets From Area -->
                                    <div class="row">
                                        <input type="hidden" name="post_id" value="{{$post->id}}">
                                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="comment"
                                                   placeholder="Add Your Comment">
                                        </div>
                                        <div class="col-md-2 p-0">
                                            <button type="submit" class="btn btn-dark">Add Comment</button>
                                        </div>
                                    </div>

                                    <!-- comments form Area -->
                                </form>
                                <!-- Start comment-sec Area -->
                                <section class="comment-sec-area pt-50 pb-60">
                                    <div class="container">
                                        @php
                                            $count = count($comments);
                                        @endphp
                                        <div class="row flex-column">
                                            <h5 class="text-uppercase pb-80">{{$count}} Comments </h5>
                                            <br>
                                            @if($comments)


                                                @foreach($comments as $c)
                                                    <div class="comment-list">
                                                        <div class="card-header single-comment justify-content-between d-flex bg-light">
                                                            <div class="user justify-content-between d-flex">
                                                                <div class="thumb">
                                                                    <img src="{{asset('images')}}/{{$c->user()->first()->userimg}}" style="height: 65px; width: 66px; border-radius: 20px" alt="#">
                                                                </div>

                                                                <div class="desc">

                                                                    <h5><a href="#">{{$c->user()->first()->fname}}</a></h5>


                                                                    <p class="date"><strong>{{$c->created_at}}</strong></p>
                                                                    <p class="comment">
                                                                       <strong class="content">{{$c->comment}}</strong>
                                                                    </p>
                                                                </div>


                                                            </div>
                                                            @if($c->user_id === Auth::user()->id)
                                                            <div class="reply-btn">
                                                                <form method="post" action="{{route('Comment.destroy' ,$c->id)}}">
                                                                    {{method_field('DELETE')}}
                                                                    {{csrf_field()}}
                                                                    <button type="submit" class="btn-danger ">
                                                                        Delete Comment
                                                                    </button>

                                                                </form>
                                                            </div>
                                                                @endif
                                                        </div>
                                                    </div>

                                                @endforeach
                                            @else <h3>NO Comment</h3>
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                </section>
                                <!-- End comment-sec Area -->


                            </div>
                    </div>


                    <div class="col-lg-4 sidebar-area ">
                        <div class="single_widget search_widget">
                            <div id="imaginary_container">
                                <div class="input-group stylish-input-group">
                                    <input type="text" class="form-control" placeholder="Search">
                                    <span class="input-group-addon">
                                        <button type="submit">
                                            <span class="lnr lnr-magnifier"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="single_widget about_widget">
                            <img src="img/asset/s-img.jpg" alt="">
                            <h2 class="text-uppercase">Adele Gonzalez</h2>
                            <p>
                                MCSE boot camps have its supporters and
                                its detractors. Some people do not understand why you should have to spend money
                            </p>
                            <div class="social-link">
                                <a href="#">
                                    <button class="btn"><i class="fa fa-facebook" aria-hidden="true"></i> Like</button>
                                </a>
                                <a href="#">
                                    <button class="btn"><i class="fa fa-twitter" aria-hidden="true"></i> follow</button>
                                </a>
                            </div>
                        </div>
                        <div class="single_widget cat_widget">
                            <h4 class="text-uppercase pb-20">post categories</h4>
                            <ul>
                                <li>
                                    <a href="#">Technology <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Lifestyle <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Fashion <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Art <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Food <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Architecture <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Adventure <span>37</span></a>
                                </li>
                            </ul>
                        </div>

                        <div class="single_widget recent_widget">
                            <h4 class="text-uppercase pb-20">Recent Posts</h4>
                            <div class="active-recent-carusel">
                                <div class="item">
                                    <img src="img/asset/slider.jpg" alt="">
                                    <p class="mt-20 title text-uppercase">Home Audio Recording <br>
                                        For Everyone</p>
                                    <p>02 Hours ago <span> <i class="fa fa-heart-o" aria-hidden="true"></i>
                                    06 <i class="fa fa-comment-o" aria-hidden="true"></i>02</span></p>
                                </div>
                                <div class="item">
                                    <img src="img/asset/slider.jpg" alt="">
                                    <p class="mt-20 title text-uppercase">Home Audio Recording <br>
                                        For Everyone</p>
                                    <p>02 Hours ago <span> <i class="fa fa-heart-o" aria-hidden="true"></i>
                                    06 <i class="fa fa-comment-o" aria-hidden="true"></i>02</span></p>
                                </div>
                                <div class="item">
                                    <img src="img/asset/slider.jpg" alt="">
                                    <p class="mt-20 title text-uppercase">Home Audio Recording <br>
                                        For Everyone</p>
                                    <p>02 Hours ago <span> <i class="fa fa-heart-o" aria-hidden="true"></i>
                                    06 <i class="fa fa-comment-o" aria-hidden="true"></i>02</span></p>
                                </div>
                            </div>
                        </div>


                        <div class="single_widget cat_widget">
                            <h4 class="text-uppercase pb-20">post archive</h4>
                            <ul>
                                <li>
                                    <a href="#">Dec'17 <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Nov'17 <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Oct'17 <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Sept'17 <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Aug'17 <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Jul'17 <span>37</span></a>
                                </li>
                                <li>
                                    <a href="#">Jun'17 <span>37</span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="single_widget tag_widget">
                            <h4 class="text-uppercase pb-20">Tag Clouds</h4>
                            <ul>
                                <li><a href="#">Lifestyle</a></li>
                                <li><a href="#">Art</a></li>
                                <li><a href="#">Adventure</a></li>
                                <li><a href="#">Food</a></li>
                                <li><a href="#">Technology</a></li>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Adventure</a></li>
                                <li><a href="#">Food</a></li>
                                <li><a href="#">Technology</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End post Area -->
    </div>



    <script>
     $('.showModal').click( function () {
         $('#myModal').show();
     });

    </script>
    <!-- End post Area -->





{{--    <script>--}}
{{--    $('#modal').on('shown.bs.modal', function () {--}}
{{--        $('##myModal').trigger('focus')--}}
{{--    })--}}
{{--</script>--}}
@endsection